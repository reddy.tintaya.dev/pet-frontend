import React, { useState } from "react";
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Container,
  Box,
  Center,
  Button,
  Heading,
  InputGroup,
  InputRightElement,
  Stack,
  Text,
  useToast,
} from "@chakra-ui/react";

import { Link, useNavigate } from "react-router-dom";

import axios from "axios";

export const SignUp = () => {
  const toast = useToast();
  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState();
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);

  const url = "http://localhost:8000/api/v1/users/signup/";

  const create = () => {

    const data = {
      username: userName,
      first_name: firstName,
      last_name: lastName,
      email: email,
      password: password,
      phone_number: phone
    };

    axios({
      method: "post",
      url: url,
      data: data,
    }).then(function (response) {
      if (response.status == 201) {
        toast({
          title: "Cuenta Creada Exitosamente",
          description: "Ya puedes iniciar sesión.",
          status: "success",
          duration: 5000,
          isClosable: true,
        });

        navigate("/login", {replace: true})
    } else {
        toast({
          title: "Error Creando Cuenta",
          description: response.data,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }).catch(
        function (error) {
            const usernameError = error.response.data.username;
            const message2 = usernameError ? usernameError : "Uno de los campos es invalido";
            toast({
                title: "Error Creando Cuenta",
                description: message2,
                status: "error",
                duration: 9000,
                isClosable: true,
              });
        }
    );
  };

  return (
    <Center>
      <Box m="20" centerContent w="40%">
        <Heading align="left"> Crear Cuenta </Heading>
        <FormControl mt="10" isRequired>
          <FormLabel htmlFor="userName" mt="10">
            Nombre de Usuario
          </FormLabel>
          <Input
            id="userName"
            type="text"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
          <FormHelperText align="left">
            No lo olvide, es el nombre de usuario que usara para inciar sesión.
          </FormHelperText>
          <FormLabel htmlFor="userName" mt="10">
            Nombre(s)
          </FormLabel>
          <Input
            id="userName"
            type="text"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
          <FormLabel htmlFor="last_name" mt="10">
            Apellidos
          </FormLabel>
          <Input
            id="last_name"
            type="text"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
          <FormLabel htmlFor="userName" mt="10">
            Número de Whatsapp
          </FormLabel>
          <Input
            id="phone"
            type="number"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
          <FormLabel htmlFor="email" mt="10">
            Correo Electronico
          </FormLabel>
          <Input
            id="email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />

          <FormLabel htmlFor="last_name" mt="10">
            Contraseña
          </FormLabel>

          <InputGroup size="md">
            <Input
              id="password"
              type={show ? "text" : "password"}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <InputRightElement width="4rem">
              <Button
                h="1.75rem"
                size="sm"
                colorScheme="teal"
                variant="ghost"
                mr="1rem"
                onClick={handleClick}
              >
                {show ? "Ocultar" : "Mostrar"}
              </Button>
            </InputRightElement>
          </InputGroup>

          <Stack>
            <Button colorScheme="teal" mt="20" onClick={create}>
              Crear Cuenta
            </Button>
            <Text pt="5">Ya tienes una cuenta?, entonces</Text>
            <Text color="teal" pt="5">
              <Link to="/login">Inicia Sesión</Link>
            </Text>
          </Stack>
        </FormControl>
      </Box>
    </Center>
  );
};
