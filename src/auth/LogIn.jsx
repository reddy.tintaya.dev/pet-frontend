import React, { useState, useEffect } from "react";
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Container,
  Box,
  Center,
  Button,
  Heading,
  InputGroup,
  InputRightElement,
  Text,
  Stack,
  useToast,
} from "@chakra-ui/react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { getAccessToken, setAccessToken } from "../utils";


export const LogIn = (props) => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);
  const toast = useToast();
  const navigate = useNavigate();
  const url = "http://localhost:8000/api/token/";

  const [userIsLoggedIn, setUserIsLoggedIn] = useState(getAccessToken())

  useEffect(() => {
    if(userIsLoggedIn) navigate("/", {replace: true})
  }, [userIsLoggedIn])
  

  const handleLogIn = () => {
    if(userName.length <= 2 || password.length < 5) {
      toast({
        title: "Llene todos los campos",
        status: "error",
        duration: 5000,
        isClosable: true,
      }); 
    }
    const data = {
      username: userName,
      password: password,
    };

    axios({
      method: "post",
      url: url,
      data: data,
    })
      .then(function (response) {
        if (response.status == 200) {
          toast({
            title: "Inicio de sesión exitoso",
            status: "success",
            duration: 5000,
            isClosable: true,
          });
          setAccessToken(response.data["access"])
          props.setNotify(true)
          navigate("/mascotas", {replace: true})
        } else {
          toast({
            title: "Credenciales inválidas",
            status: "error",
            duration: 5000,
            isClosable: true,
          });
        }
      })
      .catch(function (error) {
        toast({
          title: "Credenciales inválidas",
          status: "error",
          duration: 5000,
          isClosable: true,
        });
      });
  };

  const handleKeyDown = (e) => {
    if (e.key == 'Enter') {
      handleLogIn()
    }
  }

  return (
    <Center>
      <Box m="20" centerContent w="40%">
        <Heading align="left"> Iniciar Sesión </Heading>
        <FormControl mt="10" isRequired>
          <FormLabel htmlFor="userName" mt="10">
            Nombre de Usuario
          </FormLabel>
          <Input
            id="userName"
            type="text"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
            onKeyDown={handleKeyDown}
          />

          <FormLabel htmlFor="last_name" mt="10">
            Contraseña
          </FormLabel>
          <InputGroup size="md">
            <Input
              id="password"
              type={show ? "text" : "password"}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              onKeyDown={handleKeyDown}
            />
            <InputRightElement width="4rem">
              <Button
                h="1.75rem"
                size="sm"
                colorScheme="teal"
                variant="ghost"
                mr="1rem"
                onClick={handleClick}
              >
                {show ? "Ocultar" : "Mostrar"}
              </Button>
            </InputRightElement>
          </InputGroup>

          <Stack>
            <Button colorScheme="teal" mt="20" onClick={handleLogIn} isDisabled={userName.length < 2 || password.length < 5}>
              Iniciar Sesión
            </Button>
            <Text pt="5">Aun no tienes cuenta?</Text>
            <Text color="teal">
              <Link to="/signup">Crea una acá</Link>
            </Text>
          </Stack>
        </FormControl>
      </Box>
    </Center>
  );
};
