import React, { useState, useEffect } from "react";
import {
  Box,
  Stack,
  Heading,
  Flex,
  Text,
  Button,
  useDisclosure,
} from "@chakra-ui/react";
import { Link, useNavigate } from "react-router-dom";
import { getAccessToken, clearLocalStorage } from "./utils";
import { HamburgerIcon } from "@chakra-ui/icons";

const Header = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const handleToggle = () => (isOpen ? onClose() : onOpen());
  const [userIsLoggedIn, setUserIsLoggedIn] = useState(getAccessToken());
  const navigate = useNavigate();

  useEffect(() => {
    setUserIsLoggedIn(getAccessToken());
  }, []);

  useEffect(() => {
    if (props.notify) {
      setUserIsLoggedIn(getAccessToken());
      props.setNotify(false)
    }
  }, [props.notify]);

  const signOut = () => {
    navigate("/login", { replace: true });
    clearLocalStorage();
    props.setNotify(true)
  };

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding={6}
      bg="teal.500"
      color="white"
      {...props}
    >
      <Flex align="center" mr={5}>
        <Heading as="h1" size="lg" letterSpacing={"tighter"}>
          Save Me
        </Heading>
      </Flex>
      {userIsLoggedIn && (
        <>
          <Box display={{ base: "block", md: "none" }} onClick={handleToggle}>
            <HamburgerIcon />
          </Box>

          <Stack
            direction={{ base: "column", md: "row" }}
            display={{ base: isOpen ? "block" : "none", md: "flex" }}
            width={{ base: "full", md: "auto" }}
            alignItems="center"
            flexGrow={1}
            mt={{ base: 4, md: 0 }}
          >
            <Link to="/mascotas">Mis mascotas</Link>
            <Link to="/mascotas-perdidas">Mascotas Perdidas</Link>
            <Link to="/reportes">Reportes</Link>
          </Stack>
        </>
      )}

      <Box
        display={{ base: isOpen ? "block" : "none", md: "block" }}
        mt={{ base: 4, md: 0 }}
      >
        {!userIsLoggedIn ? (
          <Button
            variant="outline"
            _hover={{ bg: "teal.700", borderColor: "teal.700" }}
          >
            Iniciar Sesión{" "}
          </Button>
        ) : (
          <Button
            variant="outline"
            _hover={{ bg: "teal.700", borderColor: "teal.700" }}
            onClick={() => {
              signOut();
            }}
          >
            Cerrar Sesión{" "}
          </Button>
        )}
      </Box>
    </Flex>
  );
};

export default Header;
