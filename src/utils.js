export const tokenKey = "access_token";

export const setAccessToken = (token) => {
  localStorage.setItem(tokenKey, token);
};

export const getAccessToken = () => {
  return localStorage.getItem(tokenKey);
};

export const clearLocalStorage = () => {
  localStorage.clear();
};
