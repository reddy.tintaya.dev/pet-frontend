import "./App.css";
import { PetList, PetListMissing, PetListFound } from "./pets/PetList";
import { LogIn } from "./auth/LogIn";
import { SignUp } from "./auth/SignUp";
import { Routes, Route, Link } from "react-router-dom";
import Header from "./header";
import React, { useState } from "react";

function App() {
  const missingPetsUrl = "http://localhost:8000/api/v1/pets/missing/";
  const petListUrl = "http://localhost:8000/api/v1/pets/";
  const [notify, setNotify] = useState(false);

  return (
    <div className="App">
      <Header notify={notify} setNotify={setNotify}/>
      <Routes>
        <Route path="/" element={<PetList url={petListUrl} />} />
        <Route path="mascotas" element={<PetList url={petListUrl} />} />
        <Route
          path="mascotas-perdidas"
          element={<PetListMissing url={missingPetsUrl} isMissingView={true} />}
        />
        <Route
          path="reportes"
          element={<PetListFound url={missingPetsUrl} isMissingView={true} />}
        />
        <Route path="login" element={<LogIn setNotify={setNotify}/>} />
        <Route path="signup" element={<SignUp />} />
      </Routes>
    </div>
  );
}

export default App;
