import {
  Box,
  Image,
  Badge,
  Menu,
  MenuList,
  MenuItem,
  MenuButton,
  useToast
} from "@chakra-ui/react";
import axios from "axios";
import React, { useState } from "react";
import { getAccessToken, clearLocalStorage } from "../utils";

export function PetCard(props) {
  const { pet } = props;
  const [isOpen, setIsOpen] = useState(false);
  const accessToken = getAccessToken();
  const isMissingView = props.isMissingView;
  const toast = useToast();

  const handleLoEncontre = () => {
    //  get geolocation
    navigator.geolocation.getCurrentPosition(  (position) => {
      const { latitude, longitude } = position.coords;
      const data = {
        latitude: latitude,
        longitude: longitude,
      };
      axios({
        method: "post",
        url: `http://localhost:8000/api/v1/pets/${pet.id}/report-seeing/`,
        headers: { Authorization: `Bearer ${accessToken}` },
        data: data
      })
        .then((result) => {
          console.log(result)
          const whatsappLink = `https://api.whatsapp.com/send?phone=${result.data.phone_contact}&text=Acabo de ver a ${pet.name} en ...`
          toast({
            title: "Gracias por compartirlo.",
            description: <a href={whatsappLink}>Click aqui para contactar al dueño</a>,
            status: "success",
            duration: 5000,
            isClosable: true,
            onCloseComplete: () => {console.log("toasta asdasfdasdf")}
          });
          setIsOpen(false);
          props.refresh();
        })
        .catch((error) => {
          if (error.response.status > 400) {
            clearLocalStorage();
          }
        });
    })
    console.log()
    
  }

  const handleReportMissing = () => {
    axios({
      method: "post",
      url: `http://localhost:8000/api/v1/pets/${pet.id}/report-missing/`,
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((result) => {
        setIsOpen(false);
        toast({
          title: "Podras ver esta mascota en el tab de mascotas perdidas junto al resto de mascotas perdidas en tu ciudad.",
          status: "success",
          duration: 5000,
          isClosable: true,
        });
        props.refresh();
      })
      .catch((error) => {
        if (error.response.status > 400 && error.response.status !== 404) {
          clearLocalStorage();
        }
      });
  }

  const onDelete = () => {
    const url = `http://localhost:8000/api/v1/pets/${pet.id}`;
    axios({
      method: "delete",
      url: url,
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((result) => {
        console.log(result);
        props.refresh();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <Menu>
        <MenuButton>
          <Box
            maxW="sm"
            borderWidth="1px"
            borderRadius="lg"
            overflow="hidden"
            onClick={() => {
              setIsOpen(!isOpen);
            }}
          >
            <Image src={pet.photo} />

            <Box p="6">
              <Box
                mt="1"
                fontWeight="semibold"
                as="h3"
                lineHeight="tight"
                isTruncated
              >
                {pet.name}
              </Box>
              <Box color="gray.600">{pet.age} años</Box>
              <Box color="gray.600">{pet.weight} Kg</Box>
            </Box>
          </Box>
        </MenuButton>
        <MenuList>
          {isMissingView ? (
            <>
              <MenuItem onClick={handleLoEncontre}>Lo encontre!</MenuItem>
            </>
          ) : (
            <>
              <MenuItem onClick={handleReportMissing}>Reportar como Perdido</MenuItem>
              <MenuItem onClick={onDelete}>Eliminar</MenuItem>
            </>
          )}
        </MenuList>
      </Menu>
    </>
  );
}
