import { React, useState, useEffect } from "react";
import { PetCard } from "./PetCard";
import {
  Heading,
  Container,
  Button,
  Grid,
  GridItem,
  ModalOverlay,
  Modal,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Text,
  ModalFooter,
  useDisclosure,
  FormLabel,
  Input,
  Box,
  Flex,
  useToast,
  Center,
} from "@chakra-ui/react";
import { Link as ChakraLinkg } from "@chakra-ui/react";
import { AddIcon, CalendarIcon } from "@chakra-ui/icons";
import Header from "../header";
import { getAccessToken, clearLocalStorage } from "./../utils";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { FileUploader } from "react-drag-drop-files";
import { ExternalLinkIcon } from "@chakra-ui/icons";

const fileTypes = ["JPEG", "PNG", "GIF"];

export function PetList(props) {
  const navigate = useNavigate();

  const url = props.url;
  const accessToken = getAccessToken();
  const [userIsLoggedIn, setUserIsLoggedIn] = useState(getAccessToken());
  const [pets, setPets] = useState([]);
  const refresh = () => {
    axios({
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((result) => {
        setPets(result.data);
      })
      .catch((error) => {
        if (error.response.status > 400) {
          clearLocalStorage();
        }
      });
  };

  useEffect(() => {
    refresh();
  }, []);

  useEffect(() => {
    console.log("changed");
    if (!userIsLoggedIn) navigate("/login", { replace: true });
  }, [userIsLoggedIn]);

  const OverlayOne = () => (
    <ModalOverlay
      bg="blackAlpha.300"
      backdropFilter="blur(10px) hue-rotate(90deg)"
    />
  );

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [overlay, setOverlay] = useState(<OverlayOne />);

  const AddPetModal = () => {
    const [name, setName] = useState("");
    const [weight, setWeight] = useState(0.0);
    const [startDate, setStartDate] = useState(new Date());
    const [file, setFile] = useState(null);

    const handleChange = (file) => {
      setFile(file);
    };

    const createPet = () => {
      const data = new FormData();
      data.append("name", name);
      data.append("weight", weight);
      const date = new Date(startDate);
      const formattedDate = `${date.getFullYear()}-${(date.getMonth() + 1)
        .toString()
        .padStart(2, "0")}-${date.getDay().toString().padStart(2, "0")}`;
      data.append("birth_date", formattedDate);
      data.append("photo", file[0]);

      axios({
        method: "post",
        url: url,
        headers: { Authorization: `Bearer ${accessToken}` },
        data: data,
      })
        .then((result) => {
          setPets(pets.concat(result.data));
          onClose();
        })
        .catch((error) => {
          if (error.response.status > 400) {
            clearLocalStorage();
          }
        });
    };
    const handleKeyDown = (e) => {
      if (e.key == "Enter") {
        createPet();
      }
    };
    return (
      <>
        <ModalContent>
          <ModalHeader>Añadir mascota</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {/* <Text>Custom backdrop filters!</Text> */}
            <FormLabel htmlFor="petName" mt="10">
              Nombre de la mascota
            </FormLabel>
            <Input
              id="petName"
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
              onKeyDown={handleKeyDown}
            />

            <FormLabel htmlFor="petWeight" mt="10">
              Peso de la mascota (en kg)
            </FormLabel>
            <Input
              id="petWeight"
              type="number"
              value={weight}
              onChange={(e) => setWeight(e.target.value)}
              onKeyDown={handleKeyDown}
            />
            <FormLabel htmlFor="petName" mt="10">
              Fecha de Nacimiento
            </FormLabel>
            <Flex align="center">
              <CalendarIcon mr="3" />
              <DatePicker
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                dateFormat="yyyy/MM/dd"
              />
            </Flex>
            <FileUploader
              multiple={true}
              handleChange={handleChange}
              name="file"
              types={fileTypes}
            />
            <p>
              {file ? `File name: ${file[0].name}` : "no files uploaded yet"}
            </p>
          </ModalBody>
          <ModalFooter>
            <Button onClick={onClose} mr={3} variant="ghost">
              Close
            </Button>
            <Button onClick={createPet} colorScheme="blue">
              Create
            </Button>
          </ModalFooter>
        </ModalContent>
      </>
    );
  };

  return (
    <>
      <Heading align="left" pt="5" m="10">
        {props.isMissingView ? "Mis mascotas perdidas" : "Mis mascotas"}
      </Heading>
      <Grid templateColumns="repeat(5, 1fr)" gap={6} pt="5" m="10">
        {pets.map((item, index) => (
          <PetCard
            pet={item}
            key={index}
            refresh={refresh}
            isMissingView={props.isMissingView}
          />
        ))}
      </Grid>
      {!props.isMissingView && (
        <Button
          m="10"
          leftIcon={<AddIcon />}
          colorScheme="teal"
          variant="solid"
          onClick={() => {
            setOverlay(<OverlayOne />);
            onOpen();
          }}
        >
          Añadir mascota
        </Button>
      )}
      <Modal isCentered isOpen={isOpen} onClose={onClose}>
        {overlay}
        {AddPetModal()}
      </Modal>
    </>
  );
}

export function PetListMissing(props) {
  const navigate = useNavigate();

  const url = props.url;
  const accessToken = getAccessToken();
  const [userIsLoggedIn, setUserIsLoggedIn] = useState(getAccessToken());
  const [pets, setPets] = useState([]);
  const refresh = () => {
    axios({
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((result) => {
        setPets(result.data);
      })
      .catch((error) => {
        if (error.response.status > 400) {
          clearLocalStorage();
        }
      });
  };

  useEffect(() => {
    refresh();
  }, []);

  useEffect(() => {
    console.log("changed");
    if (!userIsLoggedIn) navigate("/login", { replace: true });
  }, [userIsLoggedIn]);

  const OverlayOne = () => (
    <ModalOverlay
      bg="blackAlpha.300"
      backdropFilter="blur(10px) hue-rotate(90deg)"
    />
  );

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [overlay, setOverlay] = useState(<OverlayOne />);

  const AddPetModal = () => {
    const [name, setName] = useState("");
    const [weight, setWeight] = useState(0.0);
    const [startDate, setStartDate] = useState(new Date());
    const [file, setFile] = useState(null);

    const handleChange = (file) => {
      setFile(file);
    };

    const createPet = () => {
      const data = new FormData();
      data.append("name", name);
      data.append("weight", weight);
      const date = new Date(startDate);
      const formattedDate = `${date.getFullYear()}-${(date.getMonth() + 1)
        .toString()
        .padStart(2, "0")}-${date.getDay().toString().padStart(2, "0")}`;
      data.append("birth_date", formattedDate);
      data.append("photo", file[0]);

      axios({
        method: "post",
        url: url,
        headers: { Authorization: `Bearer ${accessToken}` },
        data: data,
      })
        .then((result) => {
          setPets(pets.concat(result.data));
          onClose();
        })
        .catch((error) => {
          if (error.response.status > 400) {
            clearLocalStorage();
          }
        });
    };
    const handleKeyDown = (e) => {
      if (e.key == "Enter") {
        createPet();
      }
    };
    return (
      <>
        <ModalContent>
          <ModalHeader>Añadir mascota</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {/* <Text>Custom backdrop filters!</Text> */}
            <FormLabel htmlFor="petName" mt="10">
              Nombre de la mascota
            </FormLabel>
            <Input
              id="petName"
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
              onKeyDown={handleKeyDown}
            />

            <FormLabel htmlFor="petWeight" mt="10">
              Peso de la mascota (en kg)
            </FormLabel>
            <Input
              id="petWeight"
              type="number"
              value={weight}
              onChange={(e) => setWeight(e.target.value)}
              onKeyDown={handleKeyDown}
            />
            <FormLabel htmlFor="petName" mt="10">
              Fecha de Nacimiento
            </FormLabel>
            <Flex align="center">
              <CalendarIcon mr="3" />
              <DatePicker
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                dateFormat="yyyy/MM/dd"
              />
            </Flex>
            <FileUploader
              multiple={true}
              handleChange={handleChange}
              name="file"
              types={fileTypes}
            />
            <p>
              {file ? `File name: ${file[0].name}` : "no files uploaded yet"}
            </p>
          </ModalBody>
          <ModalFooter>
            <Button onClick={onClose} mr={3} variant="ghost">
              Close
            </Button>
            <Button onClick={createPet} colorScheme="blue">
              Create
            </Button>
          </ModalFooter>
        </ModalContent>
      </>
    );
  };

  return (
    <>
      <Heading align="left" pt="5" m="10">
        {props.isMissingView ? "Mascotas perdidas" : "Mis mascotas"}
      </Heading>
      <Grid templateColumns="repeat(5, 1fr)" gap={6} pt="5" m="10">
        {pets.map((item, index) => (
          <PetCard
            pet={item}
            key={index}
            refresh={refresh}
            isMissingView={props.isMissingView}
          />
        ))}
      </Grid>
      {!props.isMissingView && (
        <Button
          m="10"
          leftIcon={<AddIcon />}
          colorScheme="teal"
          variant="solid"
          onClick={() => {
            setOverlay(<OverlayOne />);
            onOpen();
          }}
        >
          Añadir mascota
        </Button>
      )}
      <Modal isCentered isOpen={isOpen} onClose={onClose}>
        {overlay}
        {AddPetModal()}
      </Modal>
    </>
  );
}

export const PetListFound = () => {
  const url = "http://localhost:8000/api/v1/pets/reports/";

  const accessToken = getAccessToken();
  const [results, setResults] = useState([]);

  const refresh = () => {
    axios({
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((result) => {
        console.log("results", result.data);
        setResults(result.data);
      })
      .catch((error) => {
        console.log("results", error);
        if (error.response.status > 400) {
          clearLocalStorage();
        }
      });
  };
  useEffect(() => {
    console.log("AAAAA");
    refresh();
  }, []);

  const markAsFound = (id) => {
    axios({
      method: "post",
      url: `http://localhost:8000/api/v1/pets/${id}/mark_as_found/`,
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((result) => {
        console.log("results", result.data);
        setResults(result.data);
      })
      .catch((error) => {
        console.log("results", error);
        if (error.response.status == 403) {
          clearLocalStorage();
        }
      });
  };

  return (
    <>
      <Heading align="left" pt="5" m="10">
        Reporte de Mis mascotas perdidas
      </Heading>
      {results.map((item, index) => {
        return (
          <>
            <Heading
              key={index}
              size="md"
              mt="5"
            >{`Nombre de mascota: ${item.name}`}</Heading>
            <Button mt="5" onClick={()=>{markAsFound(item.id)}}>Ya encontre a mi mascota</Button>
            {item.founds.length == 0 && (
              <Heading
                key={index + "sdf"}
                size="sm"
                color="teal.500"
                mt="5"
                mb="10"
              >
                Mascota reportada como perdida pero aún nadie la vio, vuelve mas
                tarde.
              </Heading>
            )}
            {item.founds.map((report, index2) => {
              return (
                <Center key={index2}>
                  <Box
                    maxW="sm"
                    borderWidth="1px"
                    borderRadius="lg"
                    overflow="hidden"
                    key={index2}
                    alignContent="center"
                    alignItems="center"
                    mt={5}
                    p="5"
                  >
                    {`- ${report.user.username} vio a tu mascota en: `}
                    <ChakraLinkg
                      href={`https://maps.google.com/?q=${report.latitude},${report.longitude}`}
                      isExternal
                      key={index2 + "asdf"}
                      color="teal.500"
                    >
                      Ver ubicación <ExternalLinkIcon mx="2px" />
                    </ChakraLinkg>

                    <ChakraLinkg
                      href={`https://api.whatsapp.com/send?phone=${report.user.phone}`}
                      isExternal
                      key={index2 + "asdf"}
                      color="teal.500"
                    >
                      Contactar Usuario por whatsapp{" "}
                      <ExternalLinkIcon mx="2px" />
                    </ChakraLinkg>
                  </Box>
                </Center>
              );
            })}
          </>
        );
      })}
    </>
  );
};
